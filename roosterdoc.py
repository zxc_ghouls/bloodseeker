import argparse
import re
import os
import random
import string
from docx import Document
import openpyxl
import PyPDF2

from canary.constants import AVAILABLE_DOC_TYPES

from rooster_handler.database import init_db, SESSIONLOCAL, get_token_name, get_token_email
from rooster_handler.models import Token_Internals

parser = argparse.ArgumentParser(description="Создание документов по шаблону")

doc_type_pattern = re.compile(r'^(docx|xlsx|pdf)$')

parser.add_argument("-f", "--file", dest = "file", type = str, required= False, const= None, help = "Путь к файлу созданным rooster.py" )
parser.add_argument("-d", "--doc_type", dest = "doc_type", type = str, required = True, help ="Формат документа. Доступные: docx, xlsx, pdf")
parser.add_argument("-dr","--dir", dest = "directory", type = str, required = False, const= None, help = "Путь дирректории с созданными документами сгенерированными rooster.py .")
parser.add_argument("-t","--template", dest = "template", type = str, required= True, help = "Файл-шаблон")
parser.add_argument("-n","-name", dest = "filename", type = str, required= True, help = "Шаблон для названия файла. По умолчанию файл назвается по записи в БД (если она существует, если нет - остаётся названия токена)" )

def custom_parse_args():
    args = parser.parse_args()
    
    if not doc_type_pattern.match(args.doc_type):
        parser.error(f"{args.doc_type} - Недопустимый формат файла. Доступные:{list(AVAILABLE_DOC_TYPES.keys())}")
    
    if args.file and not os.path.exists(args.file):
        parser.error(f"Файл '{args.file}' не существует.")
    
    if args.directory and not os.path.exists(args.directory):
        parser.error(f"Директория '{args.directory}' не существует.")
        
    if not os.path.exists(args.template):
        parser.error(f"Файл-шаблон '{args.template}' не существует.")
        
    return args

def extract_filename(path):
    # Регулярное выражение для извлечения названия файла без расширения
    pattern = r'\/?([^\/.]+)\.\w+$'
    match = re.search(pattern, path)
    if match:
        return match.group(1)
    else:
        return None


def generate_sendler_docx_file(email, filename_path):
    sendler_docx_path = os.path.join('sendler_conf', 'sendler.txt')
    
    with open(sendler_docx_path, 'a') as docx_file:
        docx_file.write(f"{email} {filename_path}\n")


def merge_docx(template_file, target_file, filename_path = None):
    # Открываем файл-шаблон и целевой файл
    
    template_doc = Document(template_file)
    target_doc = Document(target_file)

    # Добавляем содержимое из шаблона в целевой файл
    for element in template_doc.element.body:
        target_doc.element.body.append(element)

    # Сохраняем целевой файл, не перезаписывая его содержимое
    if filename_path:
        target_doc.save(filename_path)
    else:
        target_doc.save(target_file)


def merge_xlsx(template_file, target_file, filename_path=None):
    # Открываем файл-шаблон и целевой файл
    template_workbook = openpyxl.load_workbook(template_file)
    target_workbook = openpyxl.load_workbook(target_file)

    template_sheet = template_workbook['Sheet1']
    target_sheet = target_workbook['Sheet1']

    # Копируем содержимое из шаблона в целевой файл
    for row in template_sheet.iter_rows():
        for cell in row:
            target_cell = target_sheet.cell(row=cell.row, column=cell.column, value=cell.value)
            
            if cell.value and "INCLUDEPICTURE" in str(cell.value):

                url_match = re.search(r'INCLUDEPICTURE "(.+?)"', str(cell.value))
                if url_match:
                    url = url_match.group(1)

                    target_cell.value = f'=HYPERLINK("{url}", "Посмотреть изображение")'


    if filename_path:
        target_workbook.save(filename_path)
    else:
        target_workbook.save(target_file)

def merge_pdf(template_file, target_file, filename_path=None):

    template_pdf = PyPDF2.PdfFileReader(open(template_file, 'rb'))
    target_pdf = PyPDF2.PdfFileReader(open(target_file, 'rb'))

    merged_pdf = PyPDF2.PdfFileWriter()
    for page_num in range(template_pdf.numPages):
        page = template_pdf.getPage(page_num)
        merged_pdf.addPage(page)

    for page_num in range(target_pdf.numPages):
        page = target_pdf.getPage(page_num)
        merged_pdf.addPage(page)

    if filename_path:
        with open(filename_path, 'wb') as output_pdf:
            merged_pdf.write(output_pdf)
    else:
        with open(target_file, 'wb') as output_pdf:
            merged_pdf.write(output_pdf)


def main(args):
    if args.doc_type == "docx":
        if args.directory:
            file_list = os.listdir(args.directory)
            docx_files = [file for file in file_list if file.endswith(".docx")]
            for docx_file in docx_files:
                filepath = args.directory + docx_file
                token_value = extract_filename(docx_file)
                target_name = get_token_name(token_value)
                if target_name is not None:
                    filename_path = args.directory + target_name + "_" + args.filename + ".docx"
                else:
                    filename_path = args.directory + args.filename + "_" + docx_file[5] + ".docx"
                merge_docx(args.template, filepath, filename_path)
                email = get_token_email(token_value)
                generate_sendler_docx_file(email, filename_path)

        elif args.file:
            merge_docx(args.template, args.file)
            
            email = get_token_email(token_value)
            generate_sendler_docx_file(email, args.file)


    elif args.doc_type == "xlsx":
        if args.directory:
            file_list = os.listdir(args.directory)
            xlsx_files = [file for file in file_list if file.endswith(".xlsx")]
            for xlsx_file in xlsx_files:
                filepath = args.directory + xlsx_file
                token_value = extract_filename(xlsx_file)
                target_name = get_token_name(token_value)
                if target_name is not None:  
                    filename_path = args.directory + target_name + "_" + args.filename + ".xlsx"
                else:
                    filename_path = args.directory + args.filename + "_" + xlsx_file[5] + ".xlsx"
                merge_xlsx(args.template, filepath, filename_path)
                email = get_token_email(token_value)
                generate_sendler_docx_file(email, filename_path)
        
        elif args.file:
            merge_xlsx(args.template, args.file)
            email = get_token_email(token_value)
            generate_sendler_docx_file(email, filename_path)
    elif args.doc_type == "pdf":
            if args.directory:
                file_list = os.listdir(args.directory)
                pdf_files = [file for file in file_list if file.endswith(".pdf")]
                for pdf_file in pdf_files:
                    filepath = os.path.join(args.directory, pdf_file)
                    token_value = extract_filename(pdf_file)
                    target_name = get_token_name(token_value)
                    if target_name is not None:  
                        filename_path = os.path.join(args.directory, f"{target_name}_{args.filename}.pdf")
                    else:
                        filename_path = os.path.join(args.directory, f"{args.filename}_{pdf_file[5:]}.pdf")
                    merge_pdf(args.template, filepath, filename_path)
                    email = get_token_email(token_value)
                    generate_sendler_docx_file(email, filename_path)            
            elif args.file:
                merge_pdf(args.template, args.file)
                email = get_token_email(token_value)
                generate_sendler_docx_file(email, filename_path)


if __name__ == "__main__":
    args = custom_parse_args()
    init_db()
    main(args)
    