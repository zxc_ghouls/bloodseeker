# Rooster ()
За основу взято сердце canarytoken - генераторы файлов с отправкой get запроса на определённый сервер.  
## Подгтовка окружения
Для работы скриптов потребуется python >= 3.8  и pip.  
Все необходимые библиотеки устанавливаются командой.
```bash
pip3 install -r requirements.txt
```
## Сущности
Программное средство реализовано в виде 4 сущностей: rooster, roosterdoc, rooster_handler, rooster_sendler  
### rooster.py  
* Осуществляет генерацию файла с записанным в него url/ip адресом сервера, на который будет отправлен get запрос для скачивания кратинки(1x1 белый пиксель).  

```bash
#Пример создания docx документа
python3 -d docx -n завод1 -u 192.168.1.1:8000
```
* Создаст документ, который будет пытаться обратиться к адрессу указанному в флаге -u. Необходимо передавать адрес существующего и доступного в сети сервера rooster_handler.

* Опционально можно добавлять идентификаторы пользоваетелей флагами -w <Имя пользователя> и -e <Почта пользователя>, флаг -e необходим для отправки сообщений через smtp сервер.

* Доступна множественная генерация через текстовый файл
```bash
#Множественная генерация с помощью текстового файла
#Формат передаваемого текстового файла
#<email1> <name1>
#<email2> <name2>
python3 -d docx -n завод1 -u 192.168.1.1:8000 -j ./1.txt
```

* Описание агруметов командной строки для скрипта.
```bash
rooster.py [-h] -d DOC_TYPE -n ORG_NAME -u RESPONSE_SERVER [-w USER_NAME] [-e USER_EMAIL] [-j TARGET_JSON] [-o ORG_ID]

Создание документов с сигнализацией

options:
  -h, --help            show this help message and exit
  -d DOC_TYPE, --doc_type DOC_TYPE
                        Формат документа. Доступные: docx, xlsx, pdf
  -n ORG_NAME, --org_name ORG_NAME
                        Название организации для групировки созданных токенов
  -u RESPONSE_SERVER, --url RESPONSE_SERVER
                        IP адресс или доменное имя сервера-обработчика срабатываний. Формат: IP-адресс или доменное имя без / ( Пример: 192.168.1.1 или kukushka.org ). Убедитесь, что целевые машины
                        имеют доступ в сеть с данным сервером
  -w USER_NAME, --worker USER_NAME
                        Уникальное имя. Пример: Тамара Петровна ; Децл ; Горшок Михаил Юрьевич
  -e USER_EMAIL, --email USER_EMAIL
                        Электронная почта цели.
  -j TARGET_JSON, --json TARGET_JSON
                        Множественная генерация из файла. Аргумент: Путь до текстового файла.Формат файла: 1 email на 1 строку. Email может быть идентификатором
  -o ORG_ID, --org_id ORG_ID
                        Уникальный идентификатор организации. Формат: 4 символа чисел или латинских букв.
```
### rooster_handler
* ASGI сервер для отлавливания get запросов от созданных документов.  
* При запуске указывается порт и IP адресс(интерфейса) на котором он работает
```bash
uvicorn main:app --host <ip> --port <port> --reload 
```

### roosterdoc.py
* Утилита для добавления текста в созданные файлы и смены названия исходя из записи в БД  
```bash
# Пример использования 
python3 rosterdoc.py -d docx -t /template/template.docx -dr /generated/<doc_type>/<org_id>/ -n Отчёт
```
* Описание агруметов командной строки для скрипта.
```bash
usage: roosterdoc.py [-h] [-f FILE] -d DOC_TYPE [-dr DIRECTORY] -t TEMPLATE -n FILENAME

Создание документов по шаблону

options:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  Путь к файлу созданным rooster.py
  -d DOC_TYPE, --doc_type DOC_TYPE
                        Формат документа. Доступные: docx, xlsx, pdf
  -dr DIRECTORY, --dir DIRECTORY
                        Путь дирректории с созданными документами сгенерированными rooster.py .
  -t TEMPLATE, --template TEMPLATE
                        Файл-шаблон
  -n FILENAME, -name FILENAME
                        Шаблон для названия файла. По умолчанию файл назвается по записи в БД (если
                        она существует, если нет - остаётся названия токена)

```

### roostersendler.py 
* Отправщик документов по указанным почтовым серверам.  
* Необходимо указать в sendler_conf/config.ini параметры smtp сервера
* В файлах message и subject указать шаблон письма

```bash
# Отправка из файла, который имеет вид
# <email> <путь_до_файла>
python3 roostersendler.py -j sendler/sendler.txt 
``` 

```bash
# Отправка единичного файла на указанный адресс 
python3 roostersendler.py -e 1.mail.ru -f token.docx
```

```bash
usage: roosterdoc.py [-h] [-f FILE] -d DOC_TYPE [-dr DIRECTORY] -t TEMPLATE -n FILENAME

Создание документов по шаблону

options:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  Путь к файлу созданным rooster.py
  -d DOC_TYPE, --doc_type DOC_TYPE
                        Формат документа. Доступные: docx, xlsx, pdf
  -dr DIRECTORY, --dir DIRECTORY
                        Путь дирректории с созданными документами сгенерированными rooster.py .
  -t TEMPLATE, --template TEMPLATE
                        Файл-шаблон
  -n FILENAME, -name FILENAME
                        Шаблон для названия файла. По умолчанию файл назвается по записи в БД (если
                        она существует, если нет - остаётся названия токена)

```

## Про проект
За основу взято сердце canarytokens - генераторы документов с кастомными url адресами. Модифицирован сам токен и методы регистрации их на сервере. (pdf токен ходил по поддоменам) . Создан шаблонизатор и доставщик таких документов