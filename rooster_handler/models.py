from enum import Enum

from sqlalchemy import Column, Integer, Boolean, Text, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


# Таблица для хранения данных о хите на токен
class Token_Hit(Base):
    __tablename__ = "token_hit"

    id = Column(Integer, primary_key=True, index=True)
    token = Column(String)
    user_agent = Column(String)  # User-Agent, строка
    src_ip = Column(String)  # IP-адрес источника
    time_of_hit = Column(String)  # Время хита, строка
    request_headers = Column(Text)  # Заголовки запроса, текст
    request_args = Column(Text)  # Аргументы запроса, текст

    # Отношение к другим таблицам, если требуется
    # token_hit = relationship("SomeOtherTable")

# Таблица для хранения внутренних данных токена
class Token_Internals(Base):
    __tablename__ = "token_internals"

    id = Column(Integer, primary_key=True, index=True)
    token = Column(String)  # Токен, строка
    doc_type = Column(String)  # Тип документа, строка
    org_name = Column(String) # Наименование организации
    org_id = Column(String)  # Код организации, целое число
   
    user_email = Column(String)  # ID пользователя, целое число
    user_name = Column(String) # worker имя