"""Database for todo
"""
import os
import random
import string


from sqlalchemy import create_engine, exists
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session  #pylint: disable=unused-import
from sqlalchemy.exc import OperationalError

# pylint: disable=no-name-in-module
from . import models

DB_PATH = "./data"
DB_URL = f"sqlite:///{DB_PATH}/db.sqlite"

ENGINE = create_engine(DB_URL, connect_args={"check_same_thread": False})
# for logging all SQL-queries
#ENGINE = create_engine(DB_URL, connect_args={"check_same_thread": False}, echo=True)
SESSIONLOCAL = sessionmaker(autocommit=False, autoflush=False, bind=ENGINE)

def init_db():
    """Init database, create all models as tables
    """
    if not os.path.exists(DB_PATH):
        os.mkdir(DB_PATH)

    # check_same_thread is for SQLite only
    models.Base.metadata.create_all(bind=ENGINE)


def get_db():
    """Create session/connection for each request
    """
    database = SESSIONLOCAL()
    try:
        yield database
    finally:
        database.close()

def check_exists_org_id(org_name):
    db = SESSIONLOCAL()
    org_exists = db.query(models.Token_Internals).filter(models.Token_Internals.org_name == org_name).first()
    
    if org_exists:
        print(org_exists.org_id)  # Выведем найденное org_id
        db.close()
        return org_exists.org_id
    
    else:
        characters = string.ascii_lowercase + string.digits
        org_id = ''.join(random.choice(characters) for _ in range(4))
        db.close()
        return org_id
    
def get_token_name(token_value):
    db = SESSIONLOCAL()
    token_exists = db.query(models.Token_Internals).filter(models.Token_Internals.token == token_value).first()

    if token_exists:
        if token_exists.user_name != None:
            return token_exists.user_name

        if token_exists.user_email != None:
            return token_exists.user_email
    
    return None

def get_token_email(token_value):
    db = SESSIONLOCAL()
    token_exists = db.query(models.Token_Internals).filter(models.Token_Internals.token == token_value).first()
    
    if token_exists:
        if token_exists.user_email != None:
            return token_exists.user_email

    return None