
"""Скрипт по генерации свистков.
1. ? Обернуть в bash:
                    1. Кто отвечает за правильный ввод аргументов командной строки - 20.09 Argparse
                    2. Есть ли смысл оборачивать в bash?  - 20.09 - пока нет
2. Сейчас собрать работающий конфиг на ms и pdf - остальное можно добавить позже
3. По необходимости прикрутить базу данных для хранения свистков и сделать функцию дампа БД для дальнейшей возможной выгрузки на сервер. 20.09
4. ? SMTP для доставки документов с свистками (Возможные параметры. Адресс отправителя, и всё что внутри сообщения. Кароче подумать) 20.09
5. Обязательно ли орг_ид 20.09 ? 

На 21.09: 1. Дописать генератор урлов в соответствии с передаваемым параметром 21.09
          2. Вписать генератор доков
          3. Запуcтить сервак
          
"""
import argparse
import re
import os
import random
import string

from core import msexcel, msword, pdfgen, tokenizator
from core.constants import AVAILABLE_DOC_TYPES

from rooster_handler.database import init_db, SESSIONLOCAL, check_exists_org_id
from rooster_handler.models import Token_Internals

#Инициализация базы данных



#Регулярки для проверки варидности данных
doc_type_pattern = re.compile(r'^(docx|xlsx|pdf)$') # Можно добавить новые типы
org_id_pattern = re.compile(r'^[a-zA-Z0-9]{4}$') # 4-значное org-id
url_pattern = re.compile(r'^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d+|[a-zA-Z0-9-]+\.[a-zA-Z0-9-]+)$')
email_pattern = re.compile(r'^[\w\.-]+@[\w\.-]+$')


# Аргументы командной строки и их проверка
parser = argparse.ArgumentParser(description="Создание документов с сигнализацией")
parser.add_argument("-d", "--doc_type", dest = "doc_type", type = str, required = True, help ="Формат документа. Доступные: docx, xlsx, pdf")
parser.add_argument("-n", "--org_name", dest = "org_name", type = str, required = True, help = "Название организации для групировки созданных токенов")
parser.add_argument("-u", "--url", dest = "response_server", type = str, required = True, help = "IP адресс или доменное имя сервера-обработчика срабатываний. Формат: IP-адресс или доменное имя без \
                     / ( Пример: 192.168.1.1 или kukushka.org ). Убедитесь, что целевые машины имеют доступ в сеть с данным сервером" )
parser.add_argument("-w", "--worker", dest = "user_name", type = str, required = False, const = None, help = "Уникальное имя. Пример: Тамара Петровна ; Децл ; Горшок Михаил Юрьевич")
parser.add_argument("-e", "--email", dest = "user_email", type = str , required = False, const= None, help = "Электронная почта цели.")
parser.add_argument("-j","--json", dest = "target_json", type = str, required = False, const= None, help = "Множественная генерация из файла. Аргумент: Путь до текстового файла.Формат файла: 1 email на 1 строку. Email может быть идентификатором ")
parser.add_argument("-o", "--org_id", dest = "org_id", type = str, required = False, const= None, help = "Уникальный идентификатор организации. Формат: 4 символа чисел или латинских букв." ) #

def custom_parse_args():
    args = parser.parse_args()

    if not doc_type_pattern.match(args.doc_type):
        parser.error(f"{args.doc_type} - Недопустимый формат файла. Доступные:{list(AVAILABLE_DOC_TYPES.keys())}")
    
    if args.org_id and not org_id_pattern.match(args.org_id):
        parser.error(f"{args.org_id} - Неверный формат идентификатора организации. Формат: 4 символа из цифр и латинских букв.")
    
    if not url_pattern.match(args.response_server):
        parser.error(f"{args.response_server} - Недопустимый формат адресса или доменного имени.Форматы: <server_ip>:<port> или <name>.<domain>")

    if args.user_email and not email_pattern.match(args.user_email):
        parser.error(f"{args.user_email} - Некорректный формат email адреса.")
    
    if args.target_json:
        # Проверка на существование и доступность файла
        if not os.path.isfile(args.target_json):
            parser.error(f"{args.target_json} - Файл не существует.")
        elif not os.access(args.target_json, os.R_OK):
            parser.error(f"{args.target_json} - Нет доступа к файлу.")
    return args


def generate_org_id(org_id):
    if org_id is None:
        characters = string.ascii_lowercase + string.digits
        org_id = ''.join(random.choice(characters) for _ in range(4))
        return org_id
    else:
        return org_id

def update_template_doc():
    #regenerate func
    pass

def main(args):
    org_id = check_exists_org_id(args.org_name)
    token = tokenizator.token(doc_type=args.doc_type, org_id=org_id, url=args.response_server) # работает

    token_iternals = Token_Internals(
        token = token._value,
        doc_type = args.doc_type,
        org_name = args.org_name,
        org_id = org_id,
        user_email = args.user_email,
        user_name = args.user_name,
    )
    db = SESSIONLOCAL()
    db.add(token_iternals)
    db.commit()
    db.close()
    response_server = token.generate_url()
    
    #Проверка на существование дирректории для записи
    output_directory = f"./generated/{token._doc_type}"
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)
    
    output_directory = f"./generated/{token._doc_type}/{args.org_name}"
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)
    
    if token._doc_type == "docx":
        with open(f"./generated/{token._doc_type}/{args.org_name}/{token._value}.docx", "wb") as f: 
            f.write(
                msword.make_canary_msword(
                    url = response_server,
                    #template = "./canary/templates/template.docx"
                    template = "./canary/templates/template.docx"
                )
            )

    elif token._doc_type == "xlsx":
        with open(f"./generated/{token._doc_type}/{args.org_name}/{token._value}.xlsx", "wb") as f:
            f.write(
                msexcel.make_canary_msexcel(
                    url = response_server,
                    template = "./canary/templates/template.xlsx"
                )
            )
    elif token._doc_type == "pdf":
        with open(f"./generated/{token._doc_type}/{args.org_name}/{token._value}.pdf", "wb") as f:
            f.write(
                pdfgen.make_canary_pdf(
                    hostname = response_server.encode(),
                    template = "./canary/templates/template.pdf"
                )
            )

    #print(token._value)
    #print(AVAILABLE_DOC_TYPES.get("docx"))
    

args = custom_parse_args()
init_db()

if __name__ == "__main__":
    if args.target_json:
        with open(args.target_json, "r") as file:
            lines = file.readlines()
            for line in lines:
                parts = line.strip().split(" ")
                args.user_email = parts[0]
                args.user_name = parts[1] if len(parts) > 1 else None
                main(args)
    else:
        main(args)