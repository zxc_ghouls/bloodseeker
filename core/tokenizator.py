from __future__ import absolute_import
from typing import Optional, AnyStr, List
from canary.constants import CANARYTOKEN_ALPHABET, CANARYTOKEN_LENGTH, AVAILABLE_DOC_TYPES

import datetime
import random
import re
import shutil
import tempfile
from io import BytesIO
from pathlib import Path
from zipfile import ZipFile, ZipInfo

class NoCanarytokenFound(Exception):
    # TODO: there should be only one exception for a
    # canarytoken not found.
    pass

class token(object):
    CANARY_RE = re.compile(
        ".*(["
        + "".join(CANARYTOKEN_ALPHABET)
        + "]{"
        + str(CANARYTOKEN_LENGTH)
        + "}).*",
        re.IGNORECASE,
    )

    def __init__(self, value: Optional[AnyStr] = None, doc_type: Optional[AnyStr] = None, org_id: Optional[AnyStr] = None, url: Optional[AnyStr] = None):
        """Create a new token instance. If no value was provided,
        generate a new canarytoken.

        Arguments:
        value -- A user-provided canarytoken. It's format will be validated.

        Exceptions:
        NoCanarytokenFound - Thrown if the supplied canarytoken is not in the
                           correct format.
        """
        self._doc_type = doc_type
        self._org_id = org_id
        self._url = url
        if value:
            if isinstance(value, bytes):
                try:
                    value = value.decode()
                except UnicodeDecodeError:
                    raise NoCanarytokenFound(f"Non-decodable bytes found: {value}")
            self._value = self.find_canarytoken(value).lower()
        else:
            self._value = token.generate(self._doc_type, self._org_id)

    @staticmethod
    def generate(doc_type: Optional[AnyStr] = None, org_id: Optional[AnyStr] = None) -> str:
        """Return a new canarytoken."""
        token = "" + AVAILABLE_DOC_TYPES.get(doc_type) + org_id
        randompart = [random.choice(CANARYTOKEN_ALPHABET) for x in range(0,CANARYTOKEN_LENGTH)]
        return token+ "".join(randompart)

    @staticmethod
    def find_canarytoken(haystack: str) -> str:
        """Return the canarytoken found in haystack.

        Arguments:
        haystack -- A string that might include a canarytoken.

        Exceptions:
        NoCanarytokenFound
        """
        m = token.CANARY_RE.match(haystack)
        if not m:
            raise NoCanarytokenFound(haystack)

        return m.group(1)

    def value(
        self,
    ):
        return self._value

    def __eq__(self, other: object) -> bool:
        if isinstance(other, token):
            return self.value() == other.value()
        return False

    
    # TEST FOR MSWORD
    def generate_url(self):
        res = self._url + "/" + self._value
        return res
    
    def generate_random_hostname(self, with_random=False, nxdomain=False):
        """
        Return a hostname generated at random with the saved token.
        The random hostname is also saved into the Canarydrop.
        """
        # if nxdomain:
        #     domains = queries.get_all_canary_nxdomains()
        # else:
        #     domains = queries.get_all_canary_domains()

        if with_random:
            generated_hostname = str(random.randint(1, 2**24)) + "."
        else:
            generated_hostname = ""    

        generated_hostname += self.value() + "." # + random.choice(domains)

        return generated_hostname

    def get_hostname(self, with_random=False, as_url=False, nxdomain=False):
        random_hostname = self.generate_random_hostname(
            with_random=with_random,
            nxdomain=nxdomain,
        )
        return ("http://" if as_url else "") + random_hostname

if __name__ == "__main__":
    token = token()
    print(token._value)
    print(token.generate_url())