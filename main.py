from fastapi import FastAPI, Request, Depends, Response
from starlette.responses import Response as StarletteResponse
from datetime import datetime
from rooster_handler.database import init_db, get_db, Session
from rooster_handler.models import Token_Hit
from loguru import logger

init_db()
app = FastAPI()
logger = logger.opt(colors=True)

GIF = b"\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x80\x00\x00\xff\xff\xff\xff\xff\xff\x21\xf9\x04\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02\x4c\x01\x00\x3b" 

def get_http_general_info(request: Request):
    useragent = request.headers.get("User-Agent", "(no user-agent specified)")
    src_ip = request.headers.get("x-real-ip", request.client.host)

    src_ips = request.headers.get("x-forwarded-for", "")
    src_ip_chain = [o.strip() for o in src_ips.split(",")]

    hit_time = request.query_params.get("ts_key", datetime.utcnow().strftime("%s.%f"))

    def flatten_singletons(l):
        return l[0] if len(l) == 1 else l


    request_headers = {
        k: flatten_singletons(v)
        for k, v in request.headers.items()
    }

    request_args = {
        k: ",".join(v)
        for k, v in request.query_params.items()
    }

    return {
        "user_agent": useragent,
        "x_forwarded_for": src_ip_chain,
        "src_ip": src_ip,
        "time_of_hit": hit_time,
        "request_headers": request_headers,
        "request_args": request_args,
    }


@app.get("/{token}")
async def response_for_ms(request: Request, token: str, database: Session = Depends(get_db)):
    response = StarletteResponse(GIF)
    response.headers["Content-Type"] = "image/gif"
    http_general_info = get_http_general_info(request)
    if token != "favicon.ico":
        token_hit_data = Token_Hit(
            token = token,
            user_agent=http_general_info["user_agent"],
            src_ip=http_general_info["src_ip"],
            time_of_hit=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            request_headers=str(http_general_info["request_headers"]),
            request_args=str(http_general_info["request_args"]),
        )
        database.add(token_hit_data)
        database.commit()
        # Запись в БД + алёрт 
        logger.warning(token)
        logger.warning(http_general_info)
  
    return response

@app.get("/{token}/{pdf_trash}")
async def response_for_pdf(request: Request, token: str, database: Session = Depends(get_db)):
    response = StarletteResponse(GIF)
    response.headers["Content-Type"] = "image/gif"
    http_general_info = get_http_general_info(request)
    token_hit_data = Token_Hit(
        token = token,
        user_agent=http_general_info["user_agent"],
        src_ip=http_general_info["src_ip"],
        time_of_hit=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        request_headers=str(http_general_info["request_headers"]),
        request_args=str(http_general_info["request_args"]),
    )
    database.add(token_hit_data)
    database.commit()
    # Запись в БД + алёрт 
    logger.info(token)
    logger.info(http_general_info)
    return response