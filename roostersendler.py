import configparser
import os
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.application import MIMEApplication
import mimetypes

def extract_filename(path):
    # Регулярное выражение для извлечения названия файла без расширения
    pattern = r'\/?([^\/.]+)\.\w+$'
    match = re.search(pattern, path)
    if match:
        return match.group(1)
    else:
        return None


def read_smtp_config(filename):
    config = configparser.ConfigParser()
    config.read(filename)

    smtp_config = config['SMTP']

    smtp_server = smtp_config.get('smtp_server')
    smtp_port = smtp_config.getint('smtp_port')
    smtp_username = smtp_config.get('smtp_username')
    smtp_password = smtp_config.get('smtp_password')

    return smtp_server, smtp_port, smtp_username, smtp_password

def read_email_config(filename):
    config = configparser.ConfigParser()
    config.read(filename)

    email_config = config['Email']

    sender_email = email_config.get('sender_email')

    return sender_email

def read_subject_and_message():
    subject_file_path = os.path.join('sendler_conf', 'subject.txt')
    message_file_path = os.path.join('sendler_conf', 'message.txt')

    with open(subject_file_path, 'r', encoding='utf-8') as subject_file:
        subject = subject_file.read()

    with open(message_file_path, 'r', encoding='utf-8') as message_file:
        message = message_file.read()

    return subject, message

def send_email(smtp_server, smtp_port, smtp_username, smtp_password, sender_email, recipient_email, subject, message, document_path):
    smtp = smtplib.SMTP(smtp_server, smtp_port)

    # Проверяем поддержку TLS на сервере
    ehlo_response = smtp.ehlo()
    tls_supported = b'STARTTLS' in ehlo_response[1]

    # Если сервер поддерживает TLS, включаем шифрование TLS
    if tls_supported:
        smtp.starttls()
        smtp.ehlo()  # Обновляем ehlo после включения TLS

    # Проверяем, нужна ли аутентификация на сервере
    if smtp_username and smtp_password:
        try:
            smtp.login(smtp_username, smtp_password)
        except smtplib.SMTPNotSupportedError:
            print("SMTP AUTH not supported by the server. Sending without authentication.")

    # Создаем письмо
   
   

    # ...

    # Создаем письмо
    msg = MIMEMultipart()
    msg['From'] = sender_email
    msg['To'] = recipient_email
    msg['Subject'] = subject

    # Определяем MIME-тип файла
    mime_type, _ = mimetypes.guess_type(document_path)

    if mime_type is None:
        mime_type = "application/octet-stream"  # Если MIME-тип не может быть определен

    # Извлекаем имя файла из пути
    file_name = os.path.basename(document_path)
    #print(type(file_name))
    # Создаем MIMEApplication с правильным MIME-типом и правильным именем файла
    with open(document_path, 'rb') as document_file:
        part = MIMEApplication(document_file.read(), _subtype=mime_type)
        part.add_header('Content-Disposition', f'attachment; filename="question.docx"')
        msg.attach(part)

    # Отправляем письмо
    smtp.sendmail(sender_email, recipient_email, msg.as_string())

    smtp.quit()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Отправщик документов по почте')
    parser.add_argument('-j', '--jobfile', help='Путь до файла, который сгенерировал roosterdoc.py (sendler.txt)', required=False)
    parser.add_argument('-e', '--email', help='Емейл на который летит документ', required=False)
    parser.add_argument('-f', '--file', help='Документ', required=False)
    args = parser.parse_args()

    # Определите путь к файлу конфигурации в директории sendler_conf
    config_file_path = os.path.join('sendler_conf', 'config.ini')

    # Используйте функции для получения конфигурации, subject и message
    smtp_server, smtp_port, smtp_username, smtp_password = read_smtp_config(config_file_path)
    sender_email = read_email_config(config_file_path)
    subject, message = read_subject_and_message()

    if args.email and args.file:
        send_email(smtp_server, smtp_port, smtp_username, smtp_password, sender_email, args.email, subject, message, args.file)

    else:

        with open(args.jobfile, 'r', encoding='utf-8') as job_file:
            for line in job_file:
                line = line.strip()
                if not line:
                    continue

                recipient_email, document_path = line.split()

                # Отправка письма с recipient_email и файлом из sendler.txt
                send_email(smtp_server, smtp_port, smtp_username, smtp_password, sender_email, recipient_email, subject, message, document_path)
